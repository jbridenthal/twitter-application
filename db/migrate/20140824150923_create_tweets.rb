class CreateTweets < ActiveRecord::Migration
  def change
    create_table :tweets do |t|
      t.integer :tweet_id
      t.string :tweet_text
      t.string :created_at
      t.integer :retweets
      t.integer :favorite_count

      t.timestamps
    end
  end
end
