class TweetsController < ApplicationController
  def new
  end

  def create
    current_user.tweet(twitter_params[:message])
  end

  def twitter_params
    params.require(:tweet).permit(:message)
  end
  
  def self.tweets_store(tweets)
	 tweets.each do |tweet|
		if Tweets.where(:tweet_id => tweet.id).blank?
			new_tweet = Tweets.new
			new_tweet.tweet_id = tweet.id
			new_tweet.tweet_text = tweet.text
			new_tweet.created_at = tweet.created_at
			new_tweet.retweets = tweet.retweet_count
			new_tweet.favorite_count = tweet.favorite_count
			new_tweet.save
		end
	 end
  
  end
end